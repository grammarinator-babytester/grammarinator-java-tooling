package nl.utwente.student.rhbvankleef;

import com.google.gson.*;
import org.antlr.v4.runtime.tree.ParseTree;
import org.jetbrains.annotations.NotNull;

import java.lang.reflect.Type;
import java.util.Collections;
import java.util.List;
import java.util.stream.IntStream;

@SuppressWarnings("unused")
public record GrammarinatorSimplifiedTree(String name, String source, List<GrammarinatorSimplifiedTree> children) {
    public GrammarinatorSimplifiedTree(final @NotNull ParseTree parseTree) {
        this(
            parseTree.getClass().getName().replaceAll("Context$", ""),
            parseTree.getChildCount() == 0 ? parseTree.getText() : "",
            IntStream.range(0, parseTree.getChildCount())
                .mapToObj(parseTree::getChild)
                .map(GrammarinatorSimplifiedTree::new)
                .toList()
        );
    }

    @Override
    public List<GrammarinatorSimplifiedTree> children() {
        return Collections.unmodifiableList(children);
    }

    public static class GsonAdapter implements JsonSerializer<GrammarinatorSimplifiedTree>, JsonDeserializer<GrammarinatorSimplifiedTree> {

        @Override
        public GrammarinatorSimplifiedTree deserialize(JsonElement jsonElement, Type type, JsonDeserializationContext jsonDeserializationContext) throws JsonParseException {
            if (jsonElement.isJsonObject()) {
                String name = jsonElement.getAsJsonObject().get("name").getAsString();
                String source = jsonElement.getAsJsonObject().get("source").getAsString();
                List<GrammarinatorSimplifiedTree> children = jsonElement.getAsJsonObject()
                        .get("children").getAsJsonArray().asList()
                        .stream()
                        .map(child -> this.deserialize(child, type, jsonDeserializationContext))
                        .toList();
                return new GrammarinatorSimplifiedTree(name, source, children);
            }
            return null;
        }

        @Override
        public JsonElement serialize(GrammarinatorSimplifiedTree grammarinatorSimplifiedTree, Type type, JsonSerializationContext jsonSerializationContext) {
            JsonObject object = new JsonObject();
            object.addProperty("name", grammarinatorSimplifiedTree.name());
            object.addProperty("src", grammarinatorSimplifiedTree.source());
            object.add("children", jsonSerializationContext.serialize(grammarinatorSimplifiedTree.children()));
            object.addProperty("critical", false);
            return object;
        }
    }

    public String asJson(Gson gson) {
        if (gson == null) {
            gson = new GsonBuilder()
                    .registerTypeAdapter(GrammarinatorSimplifiedTree.class, new GsonAdapter())
                    .create();
        }

        return gson.toJson(this);
    }
}
