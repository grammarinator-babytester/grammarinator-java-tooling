package nl.utwente.student.rhbvankleef;

import org.antlr.v4.runtime.*;
import org.antlr.v4.runtime.tree.ParseTree;

import java.io.*;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.function.Function;

public class GrammarinatorCommandline {
    public static <T extends Parser> void execute(Function<CharStream, Lexer> lexerFactory, Function<TokenStream, T> parserFactory, Function<T, ParseTree> parseTreeFactory, String[] args) throws IOException {
        String inputFile = args[0];
        Path inputFilePath = Paths.get(inputFile);
        String outputFile = args[1];
        Path outputFilePath = Paths.get(outputFile);
        try(
                InputStream is = new FileInputStream(inputFilePath.getFileName().toString());
                OutputStream os = new FileOutputStream(outputFilePath.getFileName().toString())) {
            CharStream input = CharStreams.fromStream(is);
            Lexer lexer = lexerFactory.apply(input);

            CommonTokenStream tokens = new CommonTokenStream(lexer);
            T parser = parserFactory.apply(tokens);
            ParseTree parseTree = parseTreeFactory.apply(parser);

            GrammarinatorSimplifiedTree tree = new GrammarinatorSimplifiedTree(parseTree);
            os.write(tree.asJson(null).getBytes());
        }
    }
}
