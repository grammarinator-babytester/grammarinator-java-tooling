module GrammarinatorJavaTooling {
    requires com.google.gson;
    requires org.antlr.antlr4.runtime;
    requires org.jetbrains.annotations;

    exports nl.utwente.student.rhbvankleef;
}
